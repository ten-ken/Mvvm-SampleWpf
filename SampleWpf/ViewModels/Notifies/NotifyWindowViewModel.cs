﻿using CommunityToolkit.Mvvm.ComponentModel;
using CommunityToolkit.Mvvm.Messaging;
using CommunityToolkit.Mvvm.Messaging.Messages;
using System;
using System.Windows.Controls;

namespace SampleWpf.ViewModels.Notifies
{
	public partial class NotifyWindowViewModel : ObservableRecipient,IRecipient<PropertyChangedMessage<string>>
	{
		[ObservableProperty]
		private string info;

		public SupplierListViewModel SupplierListViewModel { get; } = new SupplierListViewModel() { IsActive=true} ;
		public SupplierFormViewModel SupplierFormViewModel { get; } = new SupplierFormViewModel() { IsActive = true };

		public NotifyWindowViewModel()
        {
            IsActive = true;
			WeakReferenceMessenger.Default.Register<PropertyChangedMessage<DateTime?>>(this, ReceiveDataMsg);
        }

		private void ReceiveDataMsg(object recipient, PropertyChangedMessage<DateTime?> message)
		{
			Info = SupplierFormViewModel?.SupplierInfo.ToString();
		}

		public void Receive(PropertyChangedMessage<string> message)
		{
			Info =SupplierFormViewModel?.SupplierInfo.ToString();
		}

	}
}
