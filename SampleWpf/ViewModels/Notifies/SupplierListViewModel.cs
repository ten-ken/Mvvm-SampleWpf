﻿using CommunityToolkit.Mvvm.ComponentModel;
using CommunityToolkit.Mvvm.Input;
using CommunityToolkit.Mvvm.Messaging;
using CommunityToolkit.Mvvm.Messaging.Messages;
using SampleWpf.Models;
using System;
using System.CodeDom.Compiler;
using System.Collections.ObjectModel;
using System.Windows;

namespace SampleWpf.ViewModels.Notifies
{
	public partial class SupplierListViewModel:ObservableRecipient,IRecipient<ValueChangedMessage<OsZbSupplierInfo>>
	{

		[ObservableProperty]
		private bool allowEdit;

		[ObservableProperty]
		public Visibility border1Visibility = Visibility.Hidden;

		[ObservableProperty]
		public Visibility border2Visibility = Visibility.Visible;

		//public ObservableCollection<OsZbSupplierInfo> SupplierInfos { get; } = new();
		public ObservableCollection<string> SupplierInfos { get; } = new();


		[RelayCommand]
		public void ToggleButtonClick()
		{
			Border1Visibility = AllowEdit ? Visibility.Visible : Visibility.Hidden;
			Border2Visibility = AllowEdit ? Visibility.Hidden : Visibility.Visible;

			WeakReferenceMessenger.Default.Send(new ValueChangedMessage<bool>(AllowEdit));
		}

		public void Receive(ValueChangedMessage<OsZbSupplierInfo> message)
		{
			SupplierInfos.Add(message.Value?.ToString());
		}
	}
}
