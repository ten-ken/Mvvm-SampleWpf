﻿using CommunityToolkit.Mvvm.ComponentModel;
using CommunityToolkit.Mvvm.Input;
using CommunityToolkit.Mvvm.Messaging;
using CommunityToolkit.Mvvm.Messaging.Messages;
using SampleWpf.Models;
using System;
using System.Windows;

namespace SampleWpf.ViewModels.Notifies
{
	public partial class SupplierFormViewModel:ObservableRecipient,IRecipient<ValueChangedMessage<bool>>	
	{
		[ObservableProperty]
		[NotifyCanExecuteChangedFor(nameof(SaveCommand))]
		private bool isEnabled;


		public OsZbSupplierInfo SupplierInfo { get; } = new() { EntName = "安徽" };

		public void Receive(ValueChangedMessage<bool> message)
		{
			IsEnabled = message.Value;
			//Console.WriteLine();
		}



		[RelayCommand(CanExecute =nameof(IsEnabled))]
		public void Save()
		{
			SupplierInfo.ValidateAll();
			if (SupplierInfo.HasErrors)
			{
				MessageBox.Show("数据校验失败");
			}
			
			WeakReferenceMessenger.Default.Send(new ValueChangedMessage<OsZbSupplierInfo>(SupplierInfo));
			//Console.WriteLine("保存");

		}

	}
}
