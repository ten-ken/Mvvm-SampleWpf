﻿using CommunityToolkit.Mvvm.ComponentModel;
using System;
using System.ComponentModel.DataAnnotations;

namespace SampleWpf.ViewModels
{
	public partial class SampleValidatorViewModel : ObservableValidator
	{
		[ObservableProperty]
		[NotifyDataErrorInfo]
		[Required(ErrorMessage = "Name不能为空")]
		[MinLength(2, ErrorMessage = "最小长度不得低于2位")]
		//字符串上
		private string name;

		[ObservableProperty]
		[NotifyDataErrorInfo]
		[Range(0, 160)]
		//[DisplayName("年龄")]
		private int age;

		[ObservableProperty]
		[NotifyDataErrorInfo]
		[Required]
		private DateTime? birthDay;

		[ObservableProperty]
		[NotifyDataErrorInfo]
		//[Email]
		[RegularExpression(@"^[a-zA-Z0-9_-]+(\.[a-zA-Z0-9_-]+)*@[a-zA-Z0-9_-]+(\.[a-zA-Z0-9_-]+)+$")]
		private string? email;

	}
}
