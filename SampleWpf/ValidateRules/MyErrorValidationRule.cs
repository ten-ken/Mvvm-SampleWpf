﻿using System.Globalization;
using System.Windows.Controls;

namespace SampleWpf.ValidateRules
{
	public class MyErrorValidationRule : ValidationRule
	{
		public override ValidationResult Validate(object value, CultureInfo cultureInfo)
		{
			if (value is string str && string.IsNullOrEmpty(str))
			{
				return new ValidationResult(false, "值不得为空");
			}
			return ValidationResult.ValidResult;
		}
	}
}
