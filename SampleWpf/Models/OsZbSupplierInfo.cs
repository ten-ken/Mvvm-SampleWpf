﻿using CommunityToolkit.Mvvm.ComponentModel;
using System;
using System.ComponentModel.DataAnnotations;

namespace SampleWpf.Models
{

	[ObservableRecipient]
	public partial class OsZbSupplierInfo: ObservableValidator
	{

		public int Id { get; set; }


		[ObservableProperty]
		[NotifyPropertyChangedRecipients]
		//校验
		[NotifyDataErrorInfo]
		[Required(ErrorMessage = "不得为空")]
		[MinLength(2)]
		private string? entName;

		[ObservableProperty]
		[NotifyPropertyChangedRecipients]
		[NotifyDataErrorInfo]
		[Required]
		[MinLength(10)]
		private string? entIntroduce;

		[ObservableProperty]
		[NotifyPropertyChangedRecipients]
		private DateTime? entRegTime;

		[ObservableProperty]
		[NotifyPropertyChangedRecipients]
		private string? regCapital;

		[ObservableProperty]
		private string? regPlace;

		[ObservableProperty]
		private string? facBuildPlace;

		[ObservableProperty]
		private string? contactAddress;

		public override string? ToString()
		{
			return $"公司名称:{EntName},注册时间:{EntRegTime?.ToString("yyyy-MM-dd")}, 注册资金:{RegCapital}, 企业简介:{EntIntroduce}";
		}


		//校验所有属性
		public void ValidateAll()
		{
			ValidateAllProperties();
		}

	}
}
