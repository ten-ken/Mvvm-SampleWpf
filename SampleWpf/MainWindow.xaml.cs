﻿using SampleWpf.ViewModels;
using System.Windows;

namespace SampleWpf
{
	/// <summary>
	/// Interaction logic for MainWindow.xaml
	/// </summary>
	public partial class MainWindow : Window
	{

		public MainWindow()
		{
			InitializeComponent();
			this.DataContext = new SampleViewModel()
			{
				Name = "Test",
			};
		}


		/*	private void Button_Click(object sender, RoutedEventArgs e)
			{
				SampleViewModel? sampleViewModel = this.DataContext as SampleViewModel;
				if (sampleViewModel != null)
				{
					sampleViewModel.Name = "Tom";
				}
			}*/

	}
}
