﻿using SampleWpf.ViewModels.Notifies;
using System.Windows;

namespace SampleWpf.Views.Notifies
{
	/// <summary>
	/// NotifyWindow.xaml 的交互逻辑
	/// </summary>
	public partial class NotifyWindow : Window
	{

		public NotifyWindow()
		{
			InitializeComponent();
			this.DataContext = new NotifyWindowViewModel();
		}
	}
}
