﻿using SampleWpf.ViewModels;
using System.Windows;

namespace SampleWpf
{
	/// <summary>
	/// SampleValidatorWindow.xaml 的交互逻辑
	/// </summary>
	public partial class SampleValidatorWindow : Window
	{
		public SampleValidatorWindow()
		{
			InitializeComponent();
			DataContext = new SampleValidatorViewModel();
		}
	}
}
